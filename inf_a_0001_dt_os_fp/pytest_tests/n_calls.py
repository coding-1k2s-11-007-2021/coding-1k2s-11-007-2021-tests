from typing import Callable

from inf_a_0001_dt_os_fp.n_calls import n_calls


def test_zero_returns_itis():
    assert n_calls(0) == 'ИТИС'


def test_positive_n_calls_returns_itis():
    assert n_calls(3)()()() == 'ИТИС'


def test_positive_n_minus_one_calls_returns_function():
    assert isinstance(n_calls(3)()(), Callable)
