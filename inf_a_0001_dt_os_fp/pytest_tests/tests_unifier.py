import os
import shutil
from pathlib import Path

import pytest

from inf_a_0001_dt_os_fp.tests_unifier import unify_dirs


@pytest.fixture(params=[
    ['tests_dir_1', 'tests_dir_2', 'tests_dir_3'],
    ['tests_dir_1']
])
def create_unified_dirs(request):
    current_dir_path = os.path.dirname(os.path.abspath(__file__))
    dirs = [os.path.join(current_dir_path, i) for i in request.param]
    result_dir = os.path.join(current_dir_path, 'result_dir')
    unify_dirs(dirs, result_dir)
    yield dirs, result_dir
    shutil.rmtree(result_dir)


def test_number_of_files(create_unified_dirs):
    dir_lst, result_dir = create_unified_dirs
    assert len(os.listdir(result_dir)) == sum(
        len(list(Path(i).glob('*.in'))) + len(list(Path(i).glob('*.out')))
        for i in dir_lst
    )


def test_names_of_files(create_unified_dirs):
    dir_lst, result_dir = create_unified_dirs
    for idx, current_file_name in enumerate(sorted(os.listdir(result_dir))):
        assert f'{str((idx // 2) + 1).zfill(4)}.{"in" if idx % 2 == 0 else "out"}' == current_file_name


def test_content_of_files(create_unified_dirs):
    dir_lst, result_dir = create_unified_dirs
    for idx, current_file_name in enumerate(sorted(os.listdir(result_dir))):
        with open(os.path.join(result_dir, current_file_name), 'r', encoding='utf-8') as f:
            assert f.read() == f'{(idx // 2) + 1}{"i" if idx % 2 == 0 else "o"}'
