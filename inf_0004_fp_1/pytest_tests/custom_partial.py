import pytest

from inf_0004_fp_1.custom_partial import CustomPartial


def sum_of_5(x1, x2, x3, x4, x5):
    return x1 + x2 + x3 + x4 + x5


def test_partial_with_args_type():
    assert isinstance(CustomPartial(sum_of_5, 1), CustomPartial)


def test_partial_with_kwargs_type():
    assert isinstance(CustomPartial(sum_of_5, x1=1), CustomPartial)


def test_partial_with_no_args_type():
    assert isinstance(CustomPartial(sum_of_5), CustomPartial)


def test_partial_with_all_args_call():
    assert CustomPartial(sum_of_5, 1, 2, 3, 4, 5)() == 15


def test_partial_with_all_kwargs_call():
    assert CustomPartial(sum_of_5, x1=1, x2=2, x3=3, x4=4, x5=5)() == 15


def test_partial_with_mixed_args_call():
    assert CustomPartial(sum_of_5, 1, 2, 3, x4=4, x5=5)() == 15


def test_partial_with_all_args_call_last_passing():
    assert CustomPartial(sum_of_5, 1, 2)(3, 4, 5) == 15


def test_partial_with_all_kwargs_call_last_passing():
    assert CustomPartial(sum_of_5, x1=1, x2=2)(x3=3, x4=4, x5=5) == 15


def test_partial_with_mixed_args_call_last_passing():
    assert CustomPartial(sum_of_5, 1, 2)(3, x4=4, x5=5) == 15


def test_partial_with_all_args_call_only_last_passing():
    assert CustomPartial(sum_of_5)(1, 2, 3, 4, 5) == 15


def test_partial_with_all_kwargs_call_only_last_passing():
    assert CustomPartial(sum_of_5)(x1=1, x2=2, x3=3, x4=4, x5=5) == 15


def test_partial_with_mixed_args_call_only_last_passing():
    assert CustomPartial(sum_of_5)(1, 2, 3, x4=4, x5=5) == 15


def test_partial_with_args_many_steps():
    current_partial = CustomPartial(sum_of_5, 1)
    assert isinstance(current_partial, CustomPartial)
    current_partial = CustomPartial(current_partial, 2, 3, 4)
    assert isinstance(current_partial, CustomPartial)
    current_partial = CustomPartial(current_partial, 5)
    assert isinstance(current_partial, CustomPartial)
    assert current_partial() == 15


def test_partial_with_kwargs_many_steps():
    current_partial = CustomPartial(sum_of_5, x1=1)
    assert isinstance(current_partial, CustomPartial)
    current_partial = CustomPartial(current_partial, x2=2, x3=3, x4=4)
    assert isinstance(current_partial, CustomPartial)
    current_partial = CustomPartial(current_partial, x5=5)
    assert isinstance(current_partial, CustomPartial)
    assert current_partial() == 15


def test_partial_with_mixed_args_many_steps():
    current_partial = CustomPartial(sum_of_5, 1)
    assert isinstance(current_partial, CustomPartial)
    current_partial = CustomPartial(current_partial, 2, x3=3, x4=4)
    assert isinstance(current_partial, CustomPartial)
    current_partial = CustomPartial(current_partial, x5=5)
    assert isinstance(current_partial, CustomPartial)
    assert current_partial() == 15


def test_partial_with_args_many_steps_last_passing():
    current_partial = CustomPartial(sum_of_5, 1)
    assert isinstance(current_partial, CustomPartial)
    current_partial = CustomPartial(current_partial, 2, 3)
    assert isinstance(current_partial, CustomPartial)
    assert current_partial(4, 5) == 15


def test_partial_with_kwargs_many_steps_last_passing():
    current_partial = CustomPartial(sum_of_5, x1=1)
    assert isinstance(current_partial, CustomPartial)
    current_partial = CustomPartial(current_partial, x2=2, x3=3)
    assert isinstance(current_partial, CustomPartial)
    assert current_partial(x4=4, x5=5) == 15


def test_partial_with_mixed_args_many_steps_last_passing():
    current_partial = CustomPartial(sum_of_5, 1)
    assert isinstance(current_partial, CustomPartial)
    current_partial = CustomPartial(current_partial, 2, x3=3)
    assert isinstance(current_partial, CustomPartial)
    assert current_partial(x4=4, x5=5) == 15


def test_partial_with_no_args_call():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5)()


def test_partial_with_less_args_call():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5, 1, 2, 3, 4)()


def test_partial_with_less_kwargs_call():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5, 1, 2, 3, x4=4)()


def test_partial_with_more_args_call():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5, 1, 2, 3, 4, 5, 6)()


def test_partial_with_more_kwargs_call():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5, 1, 2, 3, x4=4, x5=5, x6=6)()


def test_partial_with_less_args_call_last_passing():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5, 1, 2)(3, 4)


def test_partial_with_less_kwargs_call_last_passing():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5, 1, 2)(3, x4=4)


def test_partial_with_more_args_call_last_passing():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5, 1, 2)(3, 4, 5, 6)


def test_partial_with_more_kwargs_call_last_passing():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5, 1, 2)(3, x4=4, x5=5, x6=6)


def test_partial_with_less_args_call_only_last_passing():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5)(1, 2, 3, 4)


def test_partial_with_less_kwargs_call_only_last_passing():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5)(1, 2, 3, x4=4)


def test_partial_with_more_args_call_only_last_passing():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5)(1, 2, 3, 4, 5, 6)


def test_partial_with_more_kwargs_call_only_last_passing():
    with pytest.raises(TypeError):
        CustomPartial(sum_of_5)(1, 2, 3, x4=4, x5=5, x6=6)
