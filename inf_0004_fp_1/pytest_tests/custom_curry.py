import pytest

from inf_0004_fp_1.custom_curry import CustomCurry


@CustomCurry
def sum_of_5(x1, x2, x3, x4, x5):
    return x1 + x2 + x3 + x4 + x5


def test_uncurried_with_args_not_working():
    assert sum_of_5(1, 2, 3, 4, 5) != 15


def test_uncurried_with_kwargs_not_working():
    assert sum_of_5(x1=1, x2=2, x3=3, x4=4, x5=5) != 15


def test_uncurried_with_mixed_args_not_working():
    assert sum_of_5(1, 2, 3, x4=4, x5=5) != 15


def test_uncurried_with_args_calling_working():
    assert sum_of_5(1, 2, 3, 4, 5)() == 15


def test_uncurried_with_kwargs_calling_working():
    assert sum_of_5(x1=1, x2=2, x3=3, x4=4, x5=5)() == 15


def test_uncurried_with_mixed_args_calling_working():
    assert sum_of_5(1, 2, x3=3, x4=4, x5=5)() == 15


def test_curry_with_args_working():
    assert sum_of_5(1)(2)(3)(4)(5)() == 15


def test_curry_with_kwargs_working():
    assert sum_of_5(x1=1)(x2=2)(x3=3)(x4=4)(x5=5)() == 15


def test_curry_with_mixed_args_working():
    assert sum_of_5(1)(2)(3)(x4=4)(x5=5)() == 15


def test_curry_with_less_args_throws_error():
    with pytest.raises(TypeError):
        sum_of_5(1)(2)(3)(4)()


def test_curry_with_less_kwargs_throws_error():
    with pytest.raises(TypeError):
        sum_of_5(x1=1)(x2=2)(x3=3)(x4=4)()


def test_curry_with_mixed_less_args_throws_errors():
    with pytest.raises(TypeError):
        sum_of_5(1)(2)(3)(x4=4)()


def test_curry_with_more_args_throws_error():
    with pytest.raises(TypeError):
        sum_of_5(1)(2)(3)(4)(5)(6)()


def test_curry_with_more_kwargs_throws_error():
    with pytest.raises(TypeError):
        sum_of_5(x1=1)(x2=2)(x3=3)(x4=4)(x5=5)(x6=6)()


def test_curry_with_mixed_more_args_throws_errors():
    with pytest.raises(TypeError):
        sum_of_5(1)(2)(3)(x4=4)(x5=5)(x6=6)()


def test_uncurried_args_and_uncurried_kwargs_working():
    assert sum_of_5(1, 2, 3)(x4=4, x5=5)() == 15


def test_uncurried_args_and_uncurried_mixed_args_working():
    assert sum_of_5(1, 2, 3)(4, x5=5)() == 15


def test_uncurried_args_and_uncurried_args_working():
    assert sum_of_5(1, 2, 3)(4, 5)() == 15


def test_uncurried__and_uncurried_kwargs_working():
    assert sum_of_5(1, 2, x3=3)(x4=4, x5=5)() == 15


def test_uncurried_kwargs_and_uncurried_kwargs_working():
    assert sum_of_5(x1=1, x2=2, x3=3)(x4=4, x5=5)() == 15


def test_uncurried_args_and_uncurried_kwargs_not_working():
    assert sum_of_5(1, 2, 3)(x4=4, x5=5) != 15


def test_uncurried_args_and_uncurried_mixed_args_not_working():
    assert sum_of_5(1, 2, 3)(4, x5=5) != 15


def test_uncurried_args_and_uncurried_args_not_working():
    assert sum_of_5(1, 2, 3)(4, 5) != 15


def test_uncurried__and_uncurried_kwargs_not_working():
    assert sum_of_5(1, 2, x3=3)(x4=4, x5=5) != 15


def test_uncurried_kwargs_and_uncurried_kwargs_not_working():
    assert sum_of_5(x1=1, x2=2, x3=3)(x4=4, x5=5) != 15


def test_uncurried_with_kwarg_before_arg_throws_error():
    with pytest.raises(TypeError):
        sum_of_5(1, x2=2)(3, 4, 5)()


def test_uncurried_with_args_between_kwargs_throws_error():
    with pytest.raises(TypeError):
        sum_of_5(1, x2=2)(3, 4, x5=5)()


def test_curry_with_args_between_kwargs_throws_error():
    with pytest.raises(TypeError):
        sum_of_5(1, x2=2)(3, 4)(x5=5)()


def test_curry_with_arg_between_kwargs_throws_error():
    with pytest.raises(TypeError):
        sum_of_5(1)(x2=2)(3)(x4=4)(5)()
