import pytest

from inf_0004_fp_1.pure_function import pure_func


def test_purity():
    lst = [[1, 2, 3], 2, 3]
    pure_func(lst, 3)
    assert lst == [[1, 2, 3], 2, 3]


def test_proper_result():
    lst = [[1, 2, 3], 2, 3]
    returned_lst = pure_func(lst, 3)
    assert returned_lst == [[1, 2, 3], 2, 3, 0, 1, 2]
