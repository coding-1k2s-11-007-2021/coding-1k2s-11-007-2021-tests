import pytest

from inf_0005_fp_2.params_decorator import parameterized


@parameterized
def reverse_decorator_with_default(reverse=False):
    def wrapper(func, *args, **kwargs):
        if reverse:
            args = args[::-1]
        return func(*args, **kwargs)
    return wrapper


def test_param_default_should_work():
    @reverse_decorator_with_default()
    def sum_and_mult(a, b, c):
        return a + b * c

    assert sum_and_mult(1, 2, 3) == 7


def test_param_not_default_arg_pass_should_work():
    @reverse_decorator_with_default(True)
    def sum_and_mult(a, b, c):
        return a + b * c

    assert sum_and_mult(1, 2, 3) == 5


def test_param_not_default_kwarg_pass_should_work():
    @reverse_decorator_with_default(reverse=True)
    def sum_and_mult(a, b, c):
        return a + b * c

    assert sum_and_mult(1, 2, 3) == 5


def test_call_decorator_without_braces():
    with pytest.raises(TypeError):
        @reverse_decorator_with_default
        def sum_and_mult(a, b, c):
            return a + b * c

        assert sum_and_mult(1, 2, 3) == 7


def test_decorator_name():
    assert parameterized.__name__ == 'parameterized'


def test_decorated_func_name():
    assert reverse_decorator_with_default.__name__ == 'reverse_decorator_with_default'
