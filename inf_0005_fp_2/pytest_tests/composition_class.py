from inf_0005_fp_2.composition_class import Composition


def test_sum_funcs_work():
    inc_chr = Composition(chr) + (lambda x: x + 1) + ord
    assert inc_chr('a') == 'b'


def test_sum_funcs_and_compositions_work():
    inc_chr = Composition(chr) + (lambda x: x + 1) + Composition(ord)
    assert inc_chr('a') == 'b'


def test_sum_assignment_func_work():
    inc_chr = Composition(chr) + (lambda x: x + 1)
    inc_chr += ord
    assert inc_chr('a') == 'b'


def test_sum_assignment_composition_work():
    inc_chr = Composition(chr) + (lambda x: x + 1)
    inc_chr += Composition(ord)
    assert inc_chr('a') == 'b'
