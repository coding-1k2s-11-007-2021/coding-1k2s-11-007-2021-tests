import pytest

from inf_0005_fp_2.check_types_decorator import check_types


@check_types
def add(a: int, b: int) -> int:
    return a + b


@check_types
def concat_num_str(a: int, b: str) -> str:
    return str(a) + b


def test_args_passing_work():
    assert add(5, 6) == 11


def test_kwargs_passing_work():
    assert add(a=5, b=6) == 11


def test_mixed_passing_work():
    assert add(5, b=6) == 11


def test_both_incorrect_args_passing_not_work():
    with pytest.raises(TypeError):
        add('5', '6')


def test_both_incorrect_kwargs_passing_not_work():
    with pytest.raises(TypeError):
        add(a='5', b='6')


def test_both_incorrect_mixed_passing_not_work():
    with pytest.raises(TypeError):
        add('5', b='6')


def test_first_incorrect_args_passing_not_work():
    with pytest.raises(TypeError):
        add('5', 6)


def test_first_incorrect_kwargs_passing_not_work():
    with pytest.raises(TypeError):
        add(a='5', b=6)


def test_first_incorrect_mixed_passing_not_work():
    with pytest.raises(TypeError):
        add('5', b=6)


def test_second_incorrect_args_passing_not_work():
    with pytest.raises(TypeError):
        add(5, '6')


def test_second_incorrect_kwargs_passing_not_work():
    with pytest.raises(TypeError):
        add(a=5, b='6')


def test_second_incorrect_mixed_passing_not_work():
    with pytest.raises(TypeError):
        add(5, b='6')


def test_different_types_args_passing_work():
    assert concat_num_str(5, '6') == '56'


def test_different_types_kwargs_passing_work():
    assert concat_num_str(a=5, b='6') == '56'


def test_different_types_kwargs_reversed_passing_work():
    assert concat_num_str(b='6', a=5) == '56'


def test_different_types_mixed_passing_work():
    assert concat_num_str(5, b='6') == '56'


def test_wrong_different_types_kwargs_reversed_passing_not_work():
    with pytest.raises(TypeError):
        concat_num_str(b=6, a='5')
